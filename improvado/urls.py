# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'update', views.update, name='update'),
    url(r'search', views.search, name='search')
]