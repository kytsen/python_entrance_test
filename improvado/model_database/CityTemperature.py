from django.db import models


class CityTemperature(models.Model):

    city = models.CharField(max_length=50)
    temperature = models.IntegerField(3)
    provider = models.CharField(max_length=50)
    time = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'city_temperature'