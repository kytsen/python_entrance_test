--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

-- Started on 2017-11-21 12:36:13 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE improvado_base;
--
-- TOC entry 2150 (class 1262 OID 16441)
-- Name: improvado_base; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE improvado_base WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE improvado_base OWNER TO postgres;

\connect improvado_base

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 16499)
-- Name: city_temperature; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE city_temperature (
    id bigint NOT NULL,
    city character varying(50) NOT NULL,
    temperature integer NOT NULL,
    provider character varying(50),
    "time" timestamp without time zone NOT NULL
);


ALTER TABLE city_temperature OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16497)
-- Name: city_weather_id_city_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE city_weather_id_city_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE city_weather_id_city_seq OWNER TO postgres;

--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 185
-- Name: city_weather_id_city_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE city_weather_id_city_seq OWNED BY city_temperature.id;


--
-- TOC entry 189 (class 1259 OID 16524)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16522)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 188
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 187 (class 1259 OID 16505)
-- Name: temperature_provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE temperature_provider (
    name character varying(50) NOT NULL
);


ALTER TABLE temperature_provider OWNER TO postgres;

--
-- TOC entry 2016 (class 2604 OID 16502)
-- Name: city_temperature id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city_temperature ALTER COLUMN id SET DEFAULT nextval('city_weather_id_city_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 16527)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2142 (class 0 OID 16499)
-- Dependencies: 186
-- Data for Name: city_temperature; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO city_temperature (id, city, temperature, provider, "time") VALUES (41, 'Tomsk', 270, 'openweather', '2017-11-21 11:33:49.222187');
INSERT INTO city_temperature (id, city, temperature, provider, "time") VALUES (42, 'London', 285, 'openweather', '2017-11-21 11:33:49.599526');
INSERT INTO city_temperature (id, city, temperature, provider, "time") VALUES (45, 'Tomsk', 270, 'openweather', '2017-11-21 11:34:15.18174');
INSERT INTO city_temperature (id, city, temperature, provider, "time") VALUES (46, 'London', 285, 'openweather', '2017-11-21 11:34:15.498605');
INSERT INTO city_temperature (id, city, temperature, provider, "time") VALUES (48, 'Tomsk', 4, 'yandex', '2017-11-21 11:34:16.089703');


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 185
-- Name: city_weather_id_city_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('city_weather_id_city_seq', 48, true);


--
-- TOC entry 2145 (class 0 OID 16524)
-- Dependencies: 189
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 188
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 1, false);


--
-- TOC entry 2143 (class 0 OID 16505)
-- Dependencies: 187
-- Data for Name: temperature_provider; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2019 (class 2606 OID 16504)
-- Name: city_temperature city_weather_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city_temperature
    ADD CONSTRAINT city_weather_pkey PRIMARY KEY (id);


--
-- TOC entry 2023 (class 2606 OID 16532)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2021 (class 2606 OID 16509)
-- Name: temperature_provider weather_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY temperature_provider
    ADD CONSTRAINT weather_providers_pkey PRIMARY KEY (name);


--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-11-21 12:36:14 CET

--
-- PostgreSQL database dump complete
--

