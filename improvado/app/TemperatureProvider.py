import json
import requests
from bs4 import BeautifulSoup
import os

from entrance_test.improvado.model_database.CityTemperature import CityTemperature

OPEN_WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?q="
YANDEX_URL = "https://yandex.ru/pogoda/"

DEBUG = True


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]


class TemperaturProvider():
    __metaclass__ = Singleton

    def get_weather_from_openweather(cities):
        appid = "650e45bad02a01b288707a9f348be49b"
        provider = "openweather"

        city_time_out = []
        for city in cities:
            r = requests.get(OPEN_WEATHER_URL + city + "&appid=" + appid)
            if r.status_code != 200:
                city_time_out.append(city)
            else:
                decoded_data = json.loads(r.text)
                temperature = decoded_data['main']['temp']
                c = CityTemperature(city = city, temperature = temperature, provider = provider)
                c.save()
        return city_time_out

    def get_weather_from_yandex(cities):
        provider = "yandex"

        # format html variable from file
        print (os.getcwd())
        file = open("improvado/sample/sample.yandex.ru.html","r")
        html = ""
        for line in file:
            html += line

        city_time_out = []
        for city in cities:
            if DEBUG:
                print("Debug mode enable, only Tomsk is saved in database from static preloaded file")
                # soup = BeautifulSoup(r.text, 'html.parser') // without file
                soup = BeautifulSoup(html, 'html.parser')

                # the current temperature is the first one temp_value
                temperature = soup.find("span", {"class": "temp__value"}).text.strip()
                c = CityTemperature(city=city, temperature=temperature, provider=provider)
                c.save()
                break
            else:
                r = requests.get(YANDEX_URL + city)
                if r.status_code != 200:
                    city_time_out.append(city)
                else:
                    soup = BeautifulSoup(html, 'html.parser')

                    # the current temperature is the first one temp_value
                    temperature = soup.find("span", {"class": "temp__value"}).text.strip()
                    c = CityTemperature(city = city, temperature = temperature, provider = provider)
                    c.save()
        return city_time_out

    def get_latest_city_temperature(city):
        latest_city_temperature = []
        data_not_found = []
        try:
            city_temperature = CityTemperature.objects.filter(city=city, provider='yandex').latest('time')
            latest_city_temperature.append(city_temperature)
        except CityTemperature.DoesNotExist:
            data_not_found.append('yandex')
        try:
            city_temperature = CityTemperature.objects.filter(city=city, provider='openweather').latest('time')
            latest_city_temperature.append(city_temperature)
        except CityTemperature.DoesNotExist:
            data_not_found.append('openweather')
        return latest_city_temperature, data_not_found