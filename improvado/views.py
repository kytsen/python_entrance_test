# -*- coding: utf-8 -*-
"""Django views app module."""

from django.shortcuts import render
from entrance_test.improvado.app.TemperatureProvider import TemperaturProvider
from django.http import JsonResponse


HTTP_CODE_ERROR = 404
HTTP_CODE_ERROR_IMPOSSIBLE_DOWNLOAD = 500
HTTP_CODE_OK = 200


def clean_data_list(list_to_clean):
    list_clean = []
    for i in list_to_clean:
        list_clean.append(''.join(e for e in i if e.isalnum()))
    return list_clean


def clean_data_string(string_to_clear):
    return ''.join(e for e in string_to_clear if e.isalnum())

# http://127.0.0.1:8000/improvado/update?cities=Tomsk+London
def update(request):
    if request.method == "GET":
        if request.GET.get('cities'):
            cities = request.GET.get('cities').split()
            print(cities)
            cities = clean_data_list(cities)
            city_time_out_open_weather = TemperaturProvider.get_weather_from_openweather(cities)
            city_time_out_yandex = TemperaturProvider.get_weather_from_yandex(cities)
            cities_from_both = list(set(city_time_out_yandex) | set(city_time_out_open_weather))
            if cities_from_both:
                info_json = {}
                for c in cities_from_both:
                    data = {c, "failed to download temperature information"}
                    if c in city_time_out_yandex:
                            data.append("yandex")
                    if c in city_time_out_open_weather:
                        data.append("open_weather")
                    info_json[c] = data
                return JsonResponse(info_json, status=HTTP_CODE_ERROR_IMPOSSIBLE_DOWNLOAD)

            else :
                return render(request, 'improvado/update.html',status=HTTP_CODE_OK)
        return render(request, 'improvado/update.html',status=HTTP_CODE_ERROR)


# http://127.0.0.1:8000/improvado/search?city=London
def search(request):
    data = {}
    status = HTTP_CODE_OK
    if request.method == "GET":
        if request.GET.get('city'):
            city = request.GET.get('city')
            city = clean_data_string(city)
            city_temperatures, data_not_found = TemperaturProvider.get_latest_city_temperature(city)
            data[city] = city
            for c in city_temperatures:
                data[c.provider] = {}
                data[c.provider]["temperature"] = str(c.temperature)
                data[c.provider]["time"] = str(c.time)
            for provider in data_not_found:
                data[provider] = {}
                data[provider]["error"] = "data not found"
                status = HTTP_CODE_ERROR

    return JsonResponse(data, status=status)


def home(request):
    return render(request, 'improvado/home.html', {})